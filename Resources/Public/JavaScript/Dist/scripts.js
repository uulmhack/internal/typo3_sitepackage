/*!
 * uulmhack-web v3.0.0 (https://uulmhack.dev)
 * Copyright 2019-2022 Marcel Kapfer
 * Licensed under the GPL-2.0-or-later license
 */

var menuToggle=document.querySelector(".menu-toggle"),navBar=document.querySelector("nav"),menuItems=document.querySelector("nav > ul");null!==menuToggle&&(menuToggle.onclick=function(e){navBar.classList.toggle("toggled"),menuItems.classList.toggle("visible")});