// Function for toggling menu

var menuToggle = document.querySelector(".menu-toggle");
var navBar = document.querySelector("nav");
var menuItems = document.querySelector("nav > ul");

if (menuToggle !== null) {
    menuToggle.onclick = function(e) {
        navBar.classList.toggle("toggled");
        menuItems.classList.toggle("visible");
    };
};
