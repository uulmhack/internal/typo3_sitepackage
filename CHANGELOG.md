# Changelog

## 3.0.0 (2022-08-17)

- [FEATURE] Styled news pagination bar (2daad08)
- [TASK] Added support for TYPO3 v11 (0b9a2f0, 0cd64bc, c679b7a)
- [TASK] Added support for EXT:news v9 (82abdd0)

## 2.3.0 (2021-10-14)

- [FEATURE] Improve display of "Image in Text" elements (c06ab8fd2030adc2f85ace561d2b27fff7f6f999)
- [FEATURE] Templates for RSS and JSON feeds (105957ab2f19e37187dde6cc97e0f343fb20417e, 8ffad8554a77a4b0978873e5d39f93ec23000fb0)
- [FEATURE] Add panel content element (601c67b669a2534945c6bf2db16bdf92f298a65e)
- [FEATURE] Define text alignments (f163b276fec00e8fef9e18cd881fc36ea96dccce)
- [FEATURE] Buttons (9d173c5df9ac096d4db5ab3bfeac71f6ad805074)
- [FEATURE] Images in news lists (9261128c6140a5417f7a1119886e08bcb2dc2b6a)
- [FEATURE] Add image to homepage header (0d1d6a38c0d6d580e2f4a82b1b1dab896a9acbe5)
- [FEATURE] Social media icons in footer (de67820687b3bb792ff2085f3e9be588e94794d7)
- [FEATURE] Dark color scheme (if requested) (1d98dcafc33d92b2dda57a15177467fbc93ceb33)
- [BUGFIX] Fix missing font loading in some browsers (ab75875b4684e7112fb9dcf50af31149a1786504)
- [BUGFIX] Keep current language when clicking on "uulmhack" in the menu bar (07bd40dee2cfc4af04f96d5eacc1d10bb6ccaf72)
- [BUGFIX] Fix missing umlaut dots in navigation bar (4dcf1f1d19f4a4d205c0d649d08094ef34353feb)

## 2.2.0 (2021-05-24)

- [FEATURE] Styled radio form elements (b2a31be)
- [BUGFIX] Add additional css background definitions for checkboxes (b039e56)
- [BUGFIX] Fix margin of selected form elements (64b1851)
- [TASK] Update remote path for watch script (7a0a48d)
- [TASK] Update repo url in package.json# 2.1.0 (ef709b3)

## 2.1.0

- [FEATURE] Define possible image positions in CSS (62d8358)
- [BUGFIX] Don't cover menu with preview of workspace hint (74b329b)
- [TASK] Switched License to GPLv2 (74aeaeb)
- [TASK] Use variables in watch script (5d15115)

## 2.0.1

- [BUGFIX] Add compiled CSS files (827cd8b)
- [TASK] Added TYPO3 extension key to composer.json (a84cdd1)

## 2.0.0

- [TASK] Added support for TYPO3 v10 (f085293, 448521d)
- [FEATURE] Complete re-design of form elements (e3a0f19)
- [FEATURE] Improve table design (bae41ba)
- [FEATURE] Smal design improvements (fe072e8)

## 1.1.2

- [BUGFIX] Styled buttons in general
- [BUGFIX] Improved form styling
- [BUGFIX] Color form error messages in red

## 1.1.1

- [BUGFIX] Removed debugging output for footer menu
- [TASK] Added extention build / zip archive to gitignore

## 1.1.0

- [FEATURE] Implemented handling of multiple languages
- [FEATURE] Switched to a MenuProcessor for the footer menu

## 1.0.0

- First public release
