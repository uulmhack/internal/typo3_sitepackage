<?php

/**
 * Extension Manager/Repository config file for ext "uulmhack_web".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'uulmhack_web',
    'description' => 'Website for the uulmhack',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'fluid_styled_content' => '11.5.0-11.5.99',
            'rte_ckeditor' => '11.5.0-11.5.99',
            'news' => '9.4.0-9.9.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Uulmhack\\Web\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Marcel Kapfer',
    'author_email' => 'opensource@mmk2410.org',
    'author_company' => '',
    'version' => '3.0.0',
];
