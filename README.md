Sitepackage for the project "uulmhack"
==============================================================

Site for the uulmhack hackathon at https://uulmhack.dev

Development follows the [Git Alligator Scheme](https://euroquis.nl//blabla/2019/08/09/git-alligator.html)

Package bootstraped using the [TYPO3 Sitepackage Builder](https://www.sitepackagebuilder.com/)
