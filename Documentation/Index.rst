.. include:: Includes.txt
.. _start:

=============================================================
uulmhack_web
=============================================================

:Version:
   |release|

:Language:
   en

:Authors:
   uulmhack Team

:Email:
   typo3.docs@uulmhack.dev

:License:
   This extension documentation is published under the
   `CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>`__ (Creative Commons)
   license

Site package for the uulmhack.

**TYPO3**

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org <https://typo3.org/>`_ .

**Community Documentation**

This documentation is community documentation for the TYPO3 extension/site package uulmhack_web.

It is maintained as part of this third party extension/site package.

If you find an error or something is missing, please:
`Report a Problem <https://gitlab.com/uulmhack/internal/typo3_sitepackage/-/issues/new>`__

**Extension Manual**

This documentation is for the TYPO3 extension <uulmhack_web>.

**Credits**

This documentation was written and is currently maintained by `Marcel Kapfer <https://mmk2410.org>`__.

**For Contributors**

You are welcome to help improve this extension.
Check it out on `GitLab <https://gitlab.com/uulmhack/internal/typo3_sitepackage>`__.

**Table of Contents**

.. toctree::
   :maxdepth: 3

   RssJson/Index
   Sitemap
