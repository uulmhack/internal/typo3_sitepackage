.. include:: ../Includes.txt

.. _rss-json:

==================
RSS and JSON feeds
==================

Starting with version 2.3 the uulmhack_web site package will include templates for providing RSS and JSON feeds using `EXT:news <https://extensions.typo3.org/extension/news>`_.

Setup
=====

These can be activated by creating an new page (`uulmhack.dev <https://uulmhack.dev>`_ uses the path ``/feed``) and giving it an own template.
For this template own setup instructions are not necessary but both uulmhack_web static template must be included:

* uulmhack-web
* uulmhack-web RSS & JSON Feed

Additionally it is expected to set some constants for a correct (or good) feed output (all values are empty by default).

* **Title** (``plugin.tx_news.rss.channel.title``): Title of the feed, included in JSON and RSS output. Also gets used by some RSS clients.
* **Description** (``plugin.tx_news.rss.channel.description``): Description of the feed, included in JSON and RSS output. Also gets used by some RSS clients.
* **Link/URL** (``plugin.tx_news.rss.channel.link``): URL of the main entry point for the website, e.g. ``https://uulmhack.dev``.
* **Language** (``plugin.tx_news.rss.channel.language``): Language of the feed content (use correct codes like ``en-us``).
* **Copyright** (``plugin.tx_news.rss.channel.copyright``): Copyright holder of the feed content.
* **Category** (``plugin.tx_news.rss.channel.category``): Category of the feed.

These constants are also available using the constants editor (category "uulmhack", subcategory "Feed") on pages where the *uulmhack-web RSS & JSON Feed* template is included.

It is also necessary to create a "News system" plugin element on that page with a "List view" display type.

For propagating the RSS feed (i.e. adding a ``<link rel="alternate" type="application/rss+xml">`` tag in ``<head>``) it is necessary to define the ``page.feed.pageid`` constant.
This is easily achived by using the constant editor and navigating to the "uulmhack" category and the "Feed" subcategory there.
As a value the page id of the earlier defined page (e.g. the one with the slug ``/feed``) should be entered.
**Attention:** The adjustment must be made by the templates *not* used for the feed endpoint (i.e. the root template) since these pages should propagate the link.

Retrieving XML
==============

With the configuration above the RSS feed can be retrieved simply by visiting the corresponding path, e.g. ``/feed``.

Retrieving JSON
===============

The JSON representation of the news is availabled by passing a ``format=json`` query parameter with a GET request to the path of the feed page (e.g. ``/feed?format=json``).

The output is like in the following example.

.. code-block:: json

   {
     "title": "uulmhack News",
     "link": "https://uulmhack.dev/",
     "description": "News Feed des uulmhack",
     "language": "de-de",
     "copyright": "uulmhack Orga-Team",
     "pubDate": "Tue, 21 Sep 2021 08:41:12 +0000",
     "generator": "TYPO3 EXT:news",
     "news": [
       {
         "uid": 30,
         "topNews": false,
         "pubDate": "Wed, 04 Aug 2021 12:41:00 +0000",
         "title": "uulmhack Stammtisch August 2021",
         "link": "https://uulmhack-typo3.ddev.site/stammtisch",
         "teaser": "Hast du uns schon vermisst? Keine Sorge wir halten unser Versprechen und deshalb gibt es diesen Sonntag (08.08) den nächsten uulmhack Stammtisch."
       },
       {
         "uid": 32,
         "topNews": false,
         "pubDate": "Wed, 07 Jul 2021 19:24:14 +0000",
         "title": "uulmhack Stammtisch Juli 2021",
         "link": "https://uulmhack-typo3.ddev.site/stammtisch",
         "teaser": "Hast du uns schon vermisst? Keine Sorge wir halten unser Versprechen und deshalb gibt es diesen Sonntag (11.07) den zweiten uulmhack Stammtisch."
       },
       {
         "uid": 26,
         "topNews": false,
         "pubDate": "Sat, 12 Jun 2021 13:48:44 +0000",
         "title": "uulmhack Stammtisch Juni 2021",
         "link": "https://uulmhack-typo3.ddev.site/stammtisch",
         "mediaUrl": "https://uulmhack.dev/fileadmin/stammtisch/news-2021-06-13.png",
         "teaser": "uulmhack ist nur zweimal im Jahr! Um dazwischen auch Hackathon-Feeling zu haben, gibts nun einen Stammtisch. Los gehts am 13.06 um 14:30 Uhr."
       },
       {
         "uid": 18,
         "topNews": false,
         "pubDate": "Sat, 17 Apr 2021 08:29:30 +0000",
         "title": "uulmhack im Sommersemester 2021",
         "link": "https://uulmhack-typo3.ddev.site/news-detail-view/uulmhack-im-sommersemester-2021",
         "teaser": "Es ist mal wieder soweit! In knapp über einem Monat steht der nächste (digitale) uulmhack an. Bist du auch wieder mit dabei? Dann auf geht&#039;s zur Anmeldung!"
       },
       {
         "uid": 14,
         "topNews": false,
         "pubDate": "Sun, 08 Nov 2020 21:10:27 +0000",
         "title": "uulmhack im Wintersemester 2020/21",
         "link": "https://uulmhack-typo3.ddev.site/news-detail-view/uulmhack-im-wintersemester-2020-21",
         "teaser": "Nicht nur der Winter, sondern auch der nächste uulmhack steht vor der Tür! Vom 20. zum 22. November ist es wieder soweit. Melde dich jetzt an!"
       },
       {
         "uid": 12,
         "topNews": false,
         "pubDate": "Fri, 22 May 2020 19:54:36 +0000",
         "title": "Zeitplan online",
         "link": "https://uulmhack-typo3.ddev.site/vortraege",
         "teaser": "Unser Zeitplan für den diesmaligen uulmhack ist online. Schau rein!"
       },
       {
         "uid": 10,
         "topNews": false,
         "pubDate": "Mon, 30 Mar 2020 16:39:21 +0000",
         "title": "Nächster uulmhack vom 22.-24. Mai 2020",
         "link": "https://uulmhack-typo3.ddev.site/news-detail-view/naechster-uulmhack-vom-22-24-mai-2020",
         "teaser": "Auch im Sommersemester wird wieder ein uulmhack stattfinden. Nur dieses mal nicht an der Uni, sondern bei dir daheim."
       },
     ]
   }
