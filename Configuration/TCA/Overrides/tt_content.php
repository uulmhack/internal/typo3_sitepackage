<?php
// Add panel content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.title',
        'uulmhack_web_panel',
        'uulmhack-web-panel-icon'
    ],
    'textmedia',
    'after'
);

// Configure the backend fields for the panel content element
$GLOBALS['TCA']['tt_content']['types']['uulmhack_web_panel'] = [
    'showitem' => '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;;general,
            header,
            bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
            tx_uulmhack_web_panel_type,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;;frames,
            --palette--;;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
    ',
    'columnsOverrides' => [
        'bodytext' => [
            'config' => [
                'enableRichtext' => true,
                'richtextConfiguration' => 'default'
            ]
        ]
    ]
];


// Add backend field for type of the panel content element
$panelType = [
    'tx_uulmhack_web_panel_type' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.type',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.type.default', 'default'],
                ['LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.type.warning', 'warning'],
                ['LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.type.danger', 'danger'],
                ['LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.type.info', 'info'],
                ['LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.type.note', 'note'],
                ['LLL:EXT:uulmhack_web/Resources/Private/Language/locallang_tca.xlf:panel.type.success', 'success'],
            ],
            'default' => 'default',
        ],
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $panelType);
