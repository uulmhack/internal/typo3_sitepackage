<?php
defined('TYPO3_MODE') || die();

call_user_func(function()
{
    /**
     * Temporary variables
     */
    $extensionKey = 'uulmhack_web';

    /**
     * Default TypoScript for uulmhack_web
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'uulmhack-web'
    );

    /**
     * Additional TypoScript for generating a RSS or JSON feed page with the help of EXT:news
     */
     \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
         $extensionKey,
         'Configuration/TypoScript/Feed',
         'uulmhack-web RSS & JSON Feed'
     );
});
