#!/bin/sh
ZIP_FILE=uulmhack_web.zip
if [ -f "$ZIP_FILE" ]; then
    rm "$ZIP_FILE"
fi
zip -r "$ZIP_FILE" ./ -x Build/\* .git/\*
